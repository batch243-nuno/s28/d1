// CRUD Operations
// Inserting Document
// Insert One
db.users.insert({
    firstName : "Test",
    lastName : "Sample",
    age : 21,
    contact : {
        phone : "0000000",
        email : "testsample@email.com"
    },
    courses : ["CSS", "JavaScript", "Python"],
    department : "none"
});

// Insert Many
db.users.insertMany([
    {
        firstName : "Stephen",
        lastName : "Hawking",
        age : "76",
        contact : {
            phone: "87654321",
            email: "stephenhawking@gmail.com"
        },
        courses : ["Python", "React", "PHP"],
        department : "none"
    },
    {
        firstName : "Neil",
        lastName : "Armstrong",
        age : "82",
        contact : {
            phone: "87654321",
            email: "neilarmstrong@gmail.com"
        },
        courses : ["React", "Laravel", "Sass"],
        department : "none"
    }
]);

// Find Method
db.users.find( {firstName : "Neil"});

// Pretty Method
db.users.find( {firstName : "Neil"}).pretty();

// Finding Multiple Parameter
db.users.find({
    lastName: "Armstrong",
    age: "82"
}).pretty();

// Updating Documents
// Single Update
db.users.insert({
    firstName : "Sample",
    lastName : "Sample",
    age: 0,
    contact: {
        phone: "0000000",
        email: "sample@email.com"
    },
    courses: [],
    department: "none"
});

db.users.updateOne(
    {
        firstName: "Updated"
    },
    {
        $set:{
            lastName: "Updated",
            age: 65,
            contact: {
                phone: "12345678",
                email: "Update@eamil.com"
            },
            courses: ["PHP", "Laravel", "HTML"],
            department: "Operations",
            status: "active"
        }
    }
);

// Updating Multiple 
db.users.updateMany(
    {
        department: "none"
    },
    {
        $set:{
            department: "HR",
        }
    }
); 

// Replace One
db.users.replaceOne(
    {
        firstName: "Updated"
    },
    {

            firstName: "Chris",
            lastName: "Mortel",
            age: 14,
            contact: {
                phone: "12345678",
                email: "chris@gmail.com"
            },
            courses: ["PHP", "Laravel", "HTML"],
            department: "Operations"

    }
);

// Delete Documents
// Delete One
db.users.insert({
    firstName: "DeleteMe"
});

db.users.deleteOne({
    firstName: "DeleteMe"
});

// Delete Many
db.users.deleteMany({
    firstName: "Chris"
});

// Advance Query
// embedded document
db.users.find({
    contact: {
        phone:"87654321",
        email: "stephenhawking@gmail.com"
    }
});
// nested field
db.users.find({
    "contact.phone": "87654321"
});
// array elements
// exact
db.users.find({
    courses : ["CSS", "JavaScript", "Python"]
});
// not so exact
db.users.find({
    courses : {$all : ["React"]}
});